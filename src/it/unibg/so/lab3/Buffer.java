package it.unibg.so.lab3;

public interface Buffer<T> {
	public void insert(T item);
	public abstract T remove();
}
