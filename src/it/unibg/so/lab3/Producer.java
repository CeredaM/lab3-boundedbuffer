package it.unibg.so.lab3;

public class Producer extends Thread {
	
	private Buffer<Integer> buffer;

	public Producer(Buffer<Integer> buffer) {
		this.buffer = buffer;
	}

	@Override
	public void run() {
		for(int i=0; i<5; i++){
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			buffer.insert(i);
	    }
	}
	
	

}
